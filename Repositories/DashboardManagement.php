<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 4/18/17
 * Time: 3:45 PM
 */

namespace SM\Report\Repositories;


use Magento\Framework\ObjectManagerInterface;
use SM\Report\Repositories\Dashboard\Chart;
use SM\XRetail\Repositories\Contract\ServiceAbstract;

/**
 * Class DashboardManagement
 *
 * @package SM\Report\Repositories
 */
class DashboardManagement extends ServiceAbstract {

    /**
     * @var array
     */
    protected $_chartInstanceType = [];

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;
    /**
     * @var \SM\XRetail\Model\ResourceModel\Outlet\CollectionFactory
     */
    private $outletCollectionFactory;

    /**
     * DashboardManagement constructor.
     *
     * @param \Magento\Framework\App\RequestInterface    $requestInterface
     * @param \SM\XRetail\Helper\DataConfig              $dataConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\ObjectManagerInterface  $objectManager
     */
    public function __construct(
        \Magento\Framework\App\RequestInterface $requestInterface,
        \SM\XRetail\Helper\DataConfig $dataConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        ObjectManagerInterface $objectManager,
        \SM\XRetail\Model\ResourceModel\Outlet\CollectionFactory $outletCollectionFactory
    ) {
        $this->objectManager           = $objectManager;
        $this->outletCollectionFactory = $outletCollectionFactory;
        parent::__construct($requestInterface, $dataConfig, $storeManager);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function dashboardChartData() {
        $data = $this->getRequestData();

        $scope     = $data->getData('scope');
        $period    = $data->getData('period');
        $dateStart = $data->getData('start_date');
        $dateEnd   = $data->getData('end_date');

        if (!$scope || !$period || !$dateStart || !$dateEnd) {
            throw new \Exception("Please define scope,period,date_start,date_end");
        }

        $listScopeChart = [
            'scope'  => $scope,
            'series' => []
        ];

        switch ($scope) {
            case 'store':
                $stores = $this->storeManager->getStores();
                foreach ($stores as $store) {
                    $_chartInstance = $this->createChartInstance();
                    $_chartInstance->getDataHelper()
                                   ->setParam('start_date', $dateStart)
                                   ->setParam('end_date', $dateEnd)
                                   ->setParam('store_id', $store->getId());
                    $listScopeChart['series'][] = [
                        'name'       => $store->getName(),
                        'chart_data' => $_chartInstance->getChartData($period)
                    ];
                }
                break;
            case 'website':
                $websites = $this->storeManager->getWebsites();
                foreach ($websites as $website) {
                    $_chartInstance = $this->createChartInstance();
                    $_chartInstance->getDataHelper()
                                   ->setParam('start_date', $dateStart)
                                   ->setParam('end_date', $dateEnd)
                                   ->setParam('website_id', $website->getId());
                    $listScopeChart['series'][] = [
                        'name'       => $website->getName(),
                        'chart_data' => $_chartInstance->getChartData($period)
                    ];
                }
                break;
            case 'outlet':
                foreach ($this->outletCollectionFactory->create() as $outlet) {
                    $_chartInstance = $this->createChartInstance();
                    $_chartInstance->getDataHelper()
                                   ->setParam('start_date', $dateStart)
                                   ->setParam('end_date', $dateEnd)
                                   ->setParam('outlet_id', $outlet->getId());
                    $listScopeChart['series'][] = [
                        'name'       => $outlet->getName(),
                        'chart_data' => $_chartInstance->getChartData($period)
                    ];
                }
                break;
        }

        return $listScopeChart;
    }


    /**
     * @return \SM\Report\Repositories\Dashboard\Chart
     */
    protected function createChartInstance() {
        /** @var \SM\Report\Repositories\Dashboard\Chart $_chartInstance */
        $_chartInstance = $this->objectManager->create('SM\Report\Repositories\Dashboard\Chart');
        $_chartInstance
            ->setAxisMaps(
                [
                    'x' => 'range',

                    'a' => 'revenue',
                    'b' => 'quantity',
                    'c' => 'customer_count',
                    'd' => 'discount',
                    'e' => 'discount_percent',
                    'f' => 'average_sales',
                ])
            ->setDataRows(['revenue', 'quantity', 'customer_count', 'discount', 'discount_percent', 'average_sales']);

        return $_chartInstance;
    }

}