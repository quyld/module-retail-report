<?php
/**
 * Created by IntelliJ IDEA.
 * User: vjcspy
 * Date: 4/20/17
 * Time: 5:02 PM
 */

namespace SM\Report\Model\ResourceModel\Order;


class Collection extends \Magento\Reports\Model\ResourceModel\Order\Collection {

    private $_discountAmountExpression;

    /**
     * @param string $range
     * @param mixed  $customStart
     * @param mixed  $customEnd
     * @param int    $isFilter
     *
     * @return $this
     */
    public function prepareSummary($range, $customStart, $customEnd, $isFilter = 0) {
        $this->_prepareSummaryReport($range, $customStart, $customEnd, $isFilter);

        return $this;
    }

    /**
     * @param $range
     * @param $customStart
     * @param $customEnd
     * @param $isFilter
     *
     * @return $this
     */
    protected function _prepareSummaryReport($range, $customStart, $customEnd, $isFilter) {
        $this->setMainTable('sales_order');
        $connection = $this->getConnection();

        /**
         * Reset all columns, because result will group only by 'created_at' field
         */
        $this->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS);

        $salesAmountExpression = $this->_getSalesAmountExpression();
        if ($isFilter == 0) {
            $this->getSelect()->columns(
                [
                    'revenue' => new \Zend_Db_Expr(
                        sprintf(
                            'SUM((%s) * %s)',
                            $salesAmountExpression,
                            $connection->getIfNullSql('main_table.base_to_global_rate', 0)
                        )
                    ),
                ]
            );
        }
        else {
            $this->getSelect()->columns(['revenue' => new \Zend_Db_Expr(sprintf('SUM(%s)', $salesAmountExpression))]);
        }

        $dateRange = $this->getDateRange($range, $customStart, $customEnd);

        $tzRangeOffsetExpression = $this->_getTZRangeOffsetExpression(
            $range,
            'created_at',
            $dateRange['from'],
            $dateRange['to']
        );

        $discountAmountExpression = $this->_getDiscountAmountExpression();
        $this->getSelect()
             ->columns(
                 [
                     'quantity'          => 'COUNT(main_table.entity_id)',
                     'range'             => $tzRangeOffsetExpression,
                     'customer_count'    => 'COUNT(DISTINCT main_table.customer_id)',
                     'discount'          => new \Zend_Db_Expr(sprintf('SUM(%s)', $discountAmountExpression)),
                     'subtotal_incl_tax' => new \Zend_Db_Expr($connection->getIfNullSql('main_table.base_subtotal_incl_tax', 0)),
                     'discount_percent'  => new \Zend_Db_Expr(
                         vsprintf(
                             'SUM(%s)/SUM(%s)',
                             [$discountAmountExpression, $connection->getIfNullSql('main_table.base_subtotal_incl_tax', 0.01)])
                     ),
                     'average_sales'     => new \Zend_Db_Expr(sprintf('SUM(%s)/COUNT(main_table.entity_id)', $salesAmountExpression)),
                     'store_id'          => 'main_table.store_id',
                     'outlet_id'         => 'main_table.outlet_id'
                 ]
             )->where(
                'main_table.state NOT IN (?)',
                [\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, \Magento\Sales\Model\Order::STATE_NEW]
            )->order(
                'range',
                \Magento\Framework\DB\Select::SQL_ASC
            )->group(
                $tzRangeOffsetExpression
            );

        $this->addFieldToFilter('created_at', $dateRange);

        return $this;
    }

    /**
     * @return mixed
     */
    protected function _getDiscountAmountExpression() {
        if (null === $this->_discountAmountExpression) {
            $connection               = $this->getConnection();
            $expressionTransferObject = new \Magento\Framework\DataObject(
                [
                    'expression' => '-(%s)',
                    'arguments'  => [
                        $connection->getIfNullSql('main_table.base_discount_amount', 0),
                    ],
                ]
            );

            $this->_eventManager->dispatch(
                'sales_prepare_discount_amount_expression',
                ['collection' => $this, 'expression_object' => $expressionTransferObject]
            );
            $this->_discountAmountExpression = vsprintf(
                $expressionTransferObject->getExpression(),
                $expressionTransferObject->getArguments()
            );
        }

        return $this->_discountAmountExpression;
    }

    /**
     * Calculate From and To dates (or times) by given period
     *
     * @param string $range
     * @param string $customStart
     * @param string $customEnd
     * @param bool   $returnObjects
     *
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getDateRange($range, $customStart, $customEnd, $returnObjects = false) {
        $dateEnd   = new \DateTime($customEnd);
        $dateStart = new \DateTime($customStart);

        // go to the end of a day
        $dateEnd->setTime(23, 59, 59);

        $dateStart->setTime(0, 0, 0);

        switch ($range) {
            case '24h':
                $dateEnd = new \DateTime($customEnd);
                $dateEnd->modify('+1 hour');
                $dateStart = clone $dateEnd;
                $dateStart->modify('-1 day');
                break;

            case '7d':
                // substract 6 days we need to include
                // only today and not hte last one from range
                $dateStart->modify('-6 days');
                break;

            case '1m':
                //$dateStart->setDate(
                //    $dateStart->format('Y'),
                //    $dateStart->format('m'),
                //    $this->_scopeConfig->getValue(
                //        'reports/dashboard/mtd_start',
                //        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                //    )
                //);
                break;

            case 'custom':
                $dateStart = $customStart ? $customStart : $dateEnd;
                $dateEnd   = $customEnd ? $customEnd : $dateEnd;
                break;

            case '1y':
            case '2y':
                $startMonthDay = explode(
                    ',',
                    $this->_scopeConfig->getValue(
                        'reports/dashboard/ytd_start',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    )
                );
                $startMonth    = isset($startMonthDay[0]) ? (int)$startMonthDay[0] : 1;
                $startDay      = isset($startMonthDay[1]) ? (int)$startMonthDay[1] : 1;
                $dateStart->setDate($dateStart->format('Y'), $startMonth, $startDay);
                if ($range == '2y') {
                    $dateStart->modify('-1 year');
                }
                break;
        }

        if ($returnObjects) {
            return [$dateStart, $dateEnd];
        }
        else {
            return ['from' => $dateStart, 'to' => $dateEnd, 'datetime' => true];
        }
    }

}